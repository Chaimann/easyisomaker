# EasyISOMaker - A script for building files into the ISO file for easier transferring

This script is a personal project aimed to make it easier to build ".iso"-files for data transferring.
It does not support options like setting some platform-exclusive boot flags for different computers, its main task is to pack a readable file for you that can be written on most of the flash drives and/or be used in a Virtual Machine as a "bridge" between your
computer and the virtual operating system.

## Are there any plans for the future?
I think, yes. There are some plans on adding more functionality to this script. That'll also depend on your feedback. This is my first global solo project so I hope it will go well, just like I planned.

## Requirements:
The only thing required is the "mkisofs" command presence in your system. This one should come by default, but it is worth checking if it's actually present.
You can do this by running:
```which mkisofs```
If not, you can find this command in the ```util-linux``` package.

## Use instruction
### (No sources directory set)
First, you need to initialize the folder that'll be used for containing all the files you want to see on an ISO you're about to create.
This process can be done via:
```./eim.sh```
Piece o'cake, isn't it?
The program will ask you to enter the name of an image you want to create. Type whatever you like and press Enter.
Next thing the program will do is check for the directory. We don't have one, so it will be created automatically.
Now it's your turn to put all the files you want. The script terminates itself at this point, preventing the system from creating the image.
After all the files are in the generated sources folder, run the script again and type the name again. If you're unsure you typed it right, copy your previous input and paste it now.
The only thing left is wait! The speed of creating an ISO depends on your hardware, so please, be patient!

### (With sources directory being present already // Update the ISO)
In this step we don't really need to initialize the sources as they're already present here. That being said, you'll have two options of actions: create an ISO or update it. The second choice is relevant when you already have the ISO but need to add some files without deleting.
This provess can be done via:
```./eim.sh```
Do all the steps mentioned above and you'll have a nice a fresh ISO file in the end!~

### Building an example ISO
I've provided an example sources you can use to build your very own ISO before trying it with your files.
Feel free to test the script~