#!/bin/sh

read -p 'iso name to create: ' ISONAME
# useless line, for testing purposes only
#echo $PWD/$ISONAME _sources | awk '{printf "%s%s\n",$1,$2}'

function isoname_resolve {
ISONAME_INTERN=$PWD/$ISONAME.iso
if test -f "$ISONAME_INTERN"; then
    echo "!!! warn : you have this image already"
    sleep 2
    echo " "
    checksourcedir
    updateiso
else
    echo "iii info : the image name you requested is not taken"
    echo "    by other images, it is safe to proceed"
    sleep 2
    echo " "
    checksourcedir
    createiso
fi
}

function checksourcedir {
SOURCEFOL=$(echo $PWD/$ISONAME _sources | awk '{printf "%s%s\n",$1,$2}')
SOURCEDIR=$SOURCEFOL
echo "debug: sourcedir = " $SOURCEDIR
if [ -d "$SOURCEDIR" ]; then
    echo "source is present"
    updateiso
else 
    echo "source is not present, creating directory"
    mkdir $SOURCEDIR
    sleep 1
    echo "directory is now created. put something inside it and run the script again."
    exit 0
fi
}

function createiso {
ISOOUT=$PWD/$ISONAME
mkisofs -stream-file-name EIS -o $ISOOUT.iso $SOURCEDIR
sleep 2
echo " "
echo "complete"
exit 0
}

function updateiso {
ISOOUT=$PWD/$ISONAME
echo " "
echo "updating the iso..."
mkisofs -stream-file-name EIS -o $ISOOUT.iso $SOURCEDIR
sleep 2
echo " "
echo "complete"
exit 0
}


# START THE WHEEL OF HELL! :D
isoname_resolve
